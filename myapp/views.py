from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    text = "<h1>Hello World !</h1><p>This is my first view !</p>"
    return HttpResponse(text)


def hello(request):
    text = "<h1>Hello World !</h1><p>This is my first view !</p>"
    return HttpResponse(text)


def result(request, number):
    text = "Le résultat de la requête est %d" % number
    return HttpResponse(text)
